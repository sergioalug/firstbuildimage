FROM python:3.9

WORKDIR /usr/src/app

COPY app.py /usr/src/app
RUN pip install --no-cache-dir numpy 
RUN pip install --no-cache-dir pandas 
RUN pip install --no-cache-dir pyyaml
